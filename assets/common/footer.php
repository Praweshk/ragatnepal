<section id="footer">
		<footer class="main-footer">
  <div class="container container--max-width-1280">
    <img class="main-footer__company-logo" src="	assets/images/blood.jpg" alt="Ragat Nepal"><br>
    <span class="main-footer__company-title">
      Ragatnepal.com &ndash; Wherever you need
    </span>
    <div class="main-footer__company-address">
      Ragatnepal.com<br>
      Anamnagar, Kathmandu<br>
      9876391024 . 98672363<br>
      01-9734477<br>
      <a href="mailto:#">ragatnepal@info.com</a>
    </div>
    <div class="main-footer__appointment-advice">
      Please call us to arrange an appointment.
    </div>
    <div class="flexbox-container flexbox-container--row flexbox-container--align-end">
      <div class="main-footer__column" align="center">
        <a class="main-footer__fb-link" href="#" target="_blank">
        	<i class="fab fa-facebook-f"></i>
     	</a>
      </div>
      <div class="main-footer__column">
        <nav class="main-footer__navigation">
          <p>
          	© All right Reversed.Ragatnepal
          </p>
        </nav>
      </div>
    </div>
    
    <div class="main-footer-map">
      <div class="main-footer-map__canvas"></div>
    </div>
    
  </div>
</footer>
	</section>