$(document).ready(function(){
	var width = $(".nav").width();
	$(".nav").find(".active").append('<div class="line"></div>');


	var this_width = $(".nav").find(".active").width();
	$(".nav").find(".active").find(".line").css({"left":"0px","width":this_width,});


	$(".nav").children("li").hover(function() {
		var k = $(this).prevAll()
		var m = k.length;
		var prev_width = 0;
		for(var i = 0 ; i< m ; i++)
		{
			prev_width = prev_width + k[i].clientWidth;
		}
		var prev_active = $(".nav").find(".active").prevAll();
		var prev_len = prev_active.length;
		var active_width = 0;
		for(var m = 0 ; m< prev_len ; m++)
		{
			active_width = active_width + prev_active[m].clientWidth;
		}
		var final_width = prev_width - active_width;
		var this_width = $(this).width();
		console.log(this_width);
		$(".nav").find(".active").find(".line").css({"left":final_width,"width":this_width,});


	}, function() {
		var this_width = $(".nav").find(".active").width();
		$(".nav").find(".active").find(".line").css({"left":"0px","width":this_width,}) ;
	});


	// $('.images-collection').slick({
	//   dots: false,
	//   infinite: true,
	//   speed: 1000,
	//   slidesToShow: 1,
	//   autoplay: false,
	//   autoplaySpeed: 2000,
	//   pauseOnHover: false,
	//   arrows: true,
	//   adaptiveHeight: false
	// });
	$(document).on('change', '.btn-file :file', function() {
		var input = $(this),
			label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
		input.trigger('fileselect', [label]);
		});

		$('.btn-file :file').on('fileselect', function(event, label) {
		    
		    var input = $(this).parents('.input-group').find(':text'),
		        log = label;
		    
		    if( input.length ) {
		        input.val(log);
		    } else {
		        if( log ) alert(log);
		    }
	    
		});
		function readURL(input) {
		    if (input.files && input.files[0]) {
		        var reader = new FileReader();
		        
		        reader.onload = function (e) {
		            $('#img-upload').attr('src', e.target.result);
		        }
		        
		        reader.readAsDataURL(input.files[0]);
		    }
		}

		$("#imgInp").change(function(){
		    readURL(this);
		}); 
});