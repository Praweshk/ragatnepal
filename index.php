<!doctype html>
<html class="no-js" lang="">
    <?php include 'assets/common/header.php';?>
    <body>
      <?php include 'assets/common/nav.php';?>
        <section id="slider">
              <div class="images-collection">
                  <div class="image-wrapper">
                        <div class="image">
                          <!-- <img class="img img-responsive" src="assets/images/donate3.png"> -->
                        </div>
                        <div class="img-text" data-aos="fade-right" data-aos-offset="200" data-aos-easing="ease-in-sine">
                          <div class="text-wrapper">
                            <div class="text-head">
                              <h2 class="h2 h2-responsive">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</h2>  
                            </div>
                            <div class="text-detail">
                              <h5 class="h5 h5-responsive" >Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                              tempor incididunt quis nostrud exercitation.</h5>
                            </div>
                            <a href="#"><button class="btn slide-btn btn-lg">Sign Up</button></a>
                          </div>
                        </div>
                        <div class="all-wrapper">
                            <div class="search-wrapper" data-aos="fade-left" data-aos-anchor="#example-anchor" data-aos-offset="500" adata-aos-duration="500">
                              <div class="main-text"><h2 class="h2 h2-responsive">Blood Donation</h2></div>
                                <div class="all-inputs">
                                    <div class="dropdown">
                                        <button class="btn dropdown-toggle" id="menu1" type="button" data-toggle="dropdown">Blood Group
                                        <span class="caret"></span></button>
                                        <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
                                          <li role="presentation"><a role="menuitem" tabindex="-1" href="#">A +</a></li>
                                          <li role="presentation"><a role="menuitem" tabindex="-1" href="#">B +</a></li>
                                          <li role="presentation"><a role="menuitem" tabindex="-1" href="#">O +</a></li>
                                          <li role="presentation"><a role="menuitem" tabindex="-1" href="#">A -</a></li>   
                                          <li role="presentation"><a role="menuitem" tabindex="-1" href="#">B -</a></li>    
                                          <li role="presentation"><a role="menuitem" tabindex="-1" href="#">AB +</a></li> 
                                          <li role="presentation"><a role="menuitem" tabindex="-1" href="#">AB -</a></li>    
                                          <li role="presentation"><a role="menuitem" tabindex="-1" href="#">O +</a></li>    
                                          <li role="presentation"><a role="menuitem" tabindex="-1" href="#">O -</a></li>    
                                        </ul>
                                      </div>
                                    <input type="text" class="form-control" placeholder="Your Location"> 
                                </div>
                                <div class="search btn btn-md">Search</div>
                            </div>
                        </div> 
                         <div class="curve-back"></div>
                  </div>
              </div>
        </section>
        <section id="about_us">
            <div class="container">
              <div class="row">
                
                <div class="col-md-7" >
                  <img class="img img-responsive" data-aos="fade-right"  src="assets/images/question.jpg">
                </div>
                <div class="col-md-5" style="overflow: hidden;">
                  <div class="banner-text-inner" data-aos="fade-left" data-aos-offset="300" adata-aos-duration="500">
                            <div class="banner-shape-wrap">
                                <div class="banner-shape-inner">
                                    <img src="assets/images/shaps1.png" alt="" class="shape shape1 rotate3d">
                                    <img src="assets/images/shaps2.png" alt="" class="shape shape2 rotate2d">
                                    <img src="assets/images/shaps3.png" alt="" class="shape shape3 rotate-2d">
                                    <img src="assets/images/shaps4.png" alt="" class="shape shape4 rotate3d">
                                    <img src="assets/images/shaps5.png" alt="" class="shape shape5 rotate2d">
                                    <img src="assets/images/shaps6.png" alt="" class="shape shape6 rotate-2d">
                                    <img src="assets/images/shaps7.png" alt="" class="shape shape7 rotate3d">
                                </div>
                            </div>

                            <h1>Donate your blood.It counts alot.</h1>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                            proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                        </div>
                </div>
              </div>
            </div>
        </section>
        <section id="form_fill">
          <div class="container">
               <div class="row">
                 <div class="col-md-7" data-aos="fade-right">
                    <form>
                  <div class="title">
                    <h2>Fill the form</h2>
                  </div>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <!-- <label for="first">First Name</label> -->
                        <input type="text" class="form-control " placeholder="First Name" id="first">
                      </div>
                    </div>
                    <!--  col-md-6   -->

                    <div class="col-md-6">
                      <div class="form-group">
                        <!-- <label for="last">Last Name</label> -->
                        <input type="text" class="form-control " placeholder="Last Name" id="last">
                      </div>
                    </div>
                    <!--  col-md-6   -->
                  </div>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <!-- <label for="company">Location</label> -->
                        <input type="text" class="form-control " placeholder="Location" id="company">
                      </div>
                    </div>
                    <!--  col-md-6   -->
                    <div class="col-md-6">
                      <div class="form-group">
                            <select class="form-control " id="sel1">
                              <option>-- Select Blood --</option>
                              <option>A +</option>
                              <option>B +</option>
                              <option>A -</option>
                              <option>B -</option>
                              <option>O +</option>
                              <option>O -</option>
                              <option>AB +</option>
                              <option>AB -</option>
                              <option>AB +</option>
                            </select>
                      </div>
                    </div>
                    <!--  col-md-6   -->
                  </div>
                  <!--  row   -->
                  <!-- <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label for="phone">Phone Number</label>
                        <input type="tel" class="form-control" id="phone" placeholder="phone">
                      </div>
                    </div>
                  </div> -->
                  <!--  row   -->


                  <div class="form-group">
                    <label class="gender" for="contact-preference">Gender</label>
                      <div class="radio">
                        <label>
                          <input type="radio" name="contact-preference" id="contact-preference" value="male" checked>Male
                        </label>
                      </div>
                      <div class="radio">
                        <label>
                          <input type="radio" name="contact-preference" id="contact-preference" value="female" checked>Female
                        </label>
                      </div>
                  </div>
                  <button type="submit" class="btn btn-md btn-primary">Submit</button>
                </form>
                 </div>
                 <div class="col-md-5" data-aos="zoom-in-up">
                   <div class="form-image">
                     <img class="img img-responsive" src="assets/images/form.png">
                   </div>
                 </div>
               </div>
          </div>
        </section>



        <script type="text/javascript" src="assets/js/jquery-3.3.1.min.js"></script>
        <script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="assets/slick/slick.min.js"></script>
        <script type="text/javascript" src="assets/js/aos.js"></script>
        <script>
          AOS.init();
        </script>
        <script type="text/javascript" src="assets/js/script.js"></script>
        <script src="https://www.google-analytics.com/analytics.js" async defer></script>
    </body>
</html>